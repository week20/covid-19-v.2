import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var qValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var question = [
    "มีไข้หรือหนาวสั่น",
    "มีอาการไอ",
    "มีอาการแน่นหน้าอก",
    "มีอาการเหนื่อยล้า",
    "ปวดกล้ามเนื้อหรือร่างกาย",
    "ปวดหัว",
    "ไม่ได้กลิ่นและรส",
    "เจ็บคอ",
    "ศัดจมูกน้ำมูกไหล",
    "คลื่นไส้อาเจียน",
    "ท้องเสีย"
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Question'),
      ),
      body: ListView(
        children: [
          CheckboxListTile(
              value: qValues[0],
              title: Text(question[0]),
              onChanged: (newValue) {
                setState(() {
                  qValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[1],
              title: Text(question[1]),
              onChanged: (newValue) {
                setState(() {
                  qValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[2],
              title: Text(question[2]),
              onChanged: (newValue) {
                setState(() {
                  qValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[3],
              title: Text(question[3]),
              onChanged: (newValue) {
                setState(() {
                  qValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[4],
              title: Text(question[4]),
              onChanged: (newValue) {
                setState(() {
                  qValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[5],
              title: Text(question[5]),
              onChanged: (newValue) {
                setState(() {
                  qValues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[6],
              title: Text(question[6]),
              onChanged: (newValue) {
                setState(() {
                  qValues[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[7],
              title: Text(question[7]),
              onChanged: (newValue) {
                setState(() {
                  qValues[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[8],
              title: Text(question[8]),
              onChanged: (newValue) {
                setState(() {
                  qValues[8] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[9],
              title: Text(question[9]),
              onChanged: (newValue) {
                setState(() {
                  qValues[9] = newValue!;
                });
              }),
          CheckboxListTile(
              value: qValues[10],
              title: Text(question[10]),
              onChanged: (newValue) {
                setState(() {
                  qValues[10] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var perfs = await SharedPreferences.getInstance();
    var strQuestionValue = perfs.getString('Question_values') ??
        '[false,false,false,false,false,false,false,false,false,false,false]';
    print(strQuestionValue.substring(1, strQuestionValue.length - 1));
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        qValues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString("Question_values", qValues.toString());
    });
  }
}
